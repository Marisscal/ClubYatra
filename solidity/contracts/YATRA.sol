// SPDX-License-Identifier: MIT LICENSE

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./IYATRA.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract YATRA is IYATRA, ERC721Enumerable, Ownable, Pausable {
    using Strings for uint256;

    // condiciones de wallets y tiempo
    uint256 public MAX_MINT = 10;
    uint256 public MAX_WL_MINT = 15;
    uint256 public WL_TIMER;

    // max cantidad de nft
    uint256 public immutable MAX_TOKENS = 20000;
    // cantidad de nfts a mintear en el mint general
    uint256 public PAID_TOKENS = 18000;
    // numero de nfts minteados
    uint16 public minted; 
    // estado de la WL
    bool public isWhiteListActive = false;    
    bool public isRevealed = false;
    bool public transferPaused = true;
    
    // mapeo para las wallet que ya mintearon
    mapping(address => uint256) public whitelistedhold;
    // mapeo para las utils de OS
    mapping(uint256 => TraitStruct) public tokenTraits;
    // mapeo de las wallets de WL
    mapping(address => bool) public whitelistedAddresses;    
    
    
    address public usdt; // setear address de USDT

    string public contractUri;
    string public baseExtension = ".png";
    string public baseUri =
        "https://gateway.pinata.cloud/ipfs/";

    bool private _reentrant = false;

    address public receptorWallet = 0x5B38Da6a701c568545dCfcB03FcB875f56beddC4;

    modifier nonReentrant() {
        require(!_reentrant, "No reentrancy");
        _reentrant = true;
        _;
        _reentrant = false;
    }

    constructor() ERC721("YATRA", "YATRA") {
        contractUri = "https://gateway.pinata.cloud/ipfs/"; // para OpenSea     
        Pausable._pause();
    } 

    /// @dev function para que OpenSea retorne la metadata
    function contractURI() external view returns (string memory) {
        return contractUri; // OpenSea
    }    

    /***EXTERNAL */

    /** Solo mintean las address whitelisteadas,
     *   solo activa durante las primeras 24h de iniciado el contrato */

    function whiteListedMint(uint256 amount) external nonReentrant {
        // 
        require(
            (isWhiteListActive),
            "whitelist no esta activa"
        );
        require(
            (WL_TIMER > block.timestamp),
            "whitelist ha finalizado"
        );
        require(
            whitelistedAddresses[msg.sender] == true,
            "No estas whitelisteado !"
        );
        require(
            whitelistedhold[msg.sender] + amount <= MAX_WL_MINT,
            "Solo 15 Nfts por address. "
        );

        require(minted + amount <= 2000, "Todos los tokens han sido minteados");

        require(amount > 0 && amount <= MAX_MINT, "Monto invalido para mintear");

        uint256 realCost = mintCost();

        IERC20(usdt).transferFrom(msg.sender, receptorWallet, realCost * amount);
        
        for (uint256 i = 0; i < amount; i++) {
            minted++;        
            whitelistedhold[msg.sender]++;
            _safeMint(address(msg.sender), minted);
        }
    }

    /**
     * minteo general de NFTs
     */
    function mint(uint256 amount) external nonReentrant whenNotPaused {

        require(tx.origin == _msgSender(), "No Smart Contracts");

        require(amount > 0, "Monto invalido para mintear");
        
        require(
            minted + amount <= PAID_TOKENS,
            "Todos los tokens han sido minteados"
        );
        
        require(
            (WL_TIMER != 0),
            "whitelist no se ha iniciado"
        );
        
        require(
            (WL_TIMER < block.timestamp),
            "whitelist no ha finalizado"
        );

        uint256 realCost = mintCost();

        IERC20(usdt).transferFrom(msg.sender, receptorWallet, realCost * amount);
        
        for (uint256 i = 0; i < amount; i++) {
            minted++;            
            _safeMint(address(msg.sender), minted);
        }
    } 

    function mintCost() public view returns (uint256 price) {
        // escala de costos
        if (minted <= 2000) return 80 * 10**18;
        else if (minted <= 10000) return 100 * 10**18; 
        else if (minted <= 20000) return 150 * 10**18;       
    }

    /***READ */

    function getTokenTraits(uint256 tokenId)
        public
        view
        override
        returns (TraitStruct memory)
    {
        if (isRevealed == false) {
            return tokenTraits[0];
        }
        return tokenTraits[tokenId];
    }

    function getPaidTokens() external view override returns (uint256) {
        return PAID_TOKENS;
    }

    function getGenPriceRank() public view returns (uint256 genCoef) {
        if (minted <= 11000) {
            return 0;
        } else if (minted <= 13000) {
            return 1;        
        }
    }

    /***ADMIN */ 

    // SETTERS

    function setContractURI(string calldata _newURI) external onlyOwner {
        contractUri = _newURI; // updatable para el marketplaces likeado de OpenSea
    }    

    /**
    en caso de sorpresas de nft URI
     */
    function revealer() public onlyOwner {
        isRevealed = true;
    }

    /**
     * updates de cantidad de tokens a la venta
     */
    function setPaidTokens(uint256 _paidTokens) external onlyOwner {
        PAID_TOKENS = _paidTokens;
    }

    /**
     * pausable / no pausable minting / por default sale pausado
     */
    function setPaused(bool _paused) external onlyOwner {
        if (_paused) _pause();
        else _unpause();
    }

    function setWhitelistActive(bool _wlEnd) external onlyOwner {
        WL_TIMER = block.timestamp + 10 minutes;
        isWhiteListActive = _wlEnd;
    }    

    function setUSDT(address _usdt) public onlyOwner {
        usdt = _usdt;
    }    

    function setMaxWLMint(uint256 _max) public onlyOwner {
        MAX_WL_MINT = _max;
    }

    function whitelistUsers(address[] calldata _users) public onlyOwner {
        for (uint256 i = 0; i < _users.length; i++) {
            whitelistedAddresses[_users[i]] = true;
        }
    }

    function walletOfOwner(address _owner)
        public
        view
        returns (uint256[] memory)
    {
        uint256 ownerTokenCount = balanceOf(_owner);
        uint256[] memory tokenIds = new uint256[](ownerTokenCount);
        for (uint256 i; i < ownerTokenCount; i++) {
            tokenIds[i] = tokenOfOwnerByIndex(_owner, i);
        }
        return tokenIds;
    }

    function balanceHolder(address user)
        external
        view
        virtual
        override
        returns (uint256)
    {
        uint256 ownerTokenCount = balanceOf(user);
        return ownerTokenCount;
    }

    function _safeTransfer(
        address from,
        address to,
        uint256 tokenId
        
    ) internal virtual {
        
        _transfer(from, to, tokenId);
    }

    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) public virtual override (ERC721, IERC721){
        
        //
        require(
            _isApprovedOrOwner(_msgSender(), tokenId),
            "ERC721: transfer caller is not owner nor approved"
        );

        _transfer(from, to, tokenId);
    }
    
    /***RENDER */
    //***  Utils para trabajar en OpenSea **** */

    function compileAttributes(uint256 tokenId)
        public
        view
        returns (string memory)
    {
        IYATRA.TraitStruct memory s = getTokenTraits(tokenId);
        string memory traits;
        if (s.isPP) {
            traits = string(
                abi.encodePacked(
                    '[{"trait_type":"Class"',
                    ",",
                    '"value":""',
                    "},"
                )
            );
        } else {
            traits = string(
                abi.encodePacked(
                    '[{"trait_type":"Class"',
                    ",",
                    '"value":""',
                    "},"
                    
                )
            );
        }
        return traits;
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override
        returns (string memory)
    {
        if (isRevealed == false) {
            return baseUri;
        }

        IYATRA.TraitStruct memory s = getTokenTraits(tokenId);

        string memory metadata = string(
            abi.encodePacked(
                '{"name": "',
                s.isPP ? " #" : " #",
                tokenId.toString(),
                '", "description": "", "image": "',
                string(
                    abi.encodePacked(                                                
                        baseExtension
                    )
                ),
                '", "attributes": ',
                compileAttributes(tokenId),
                "}"
            )
        );
        return
            string(
                abi.encodePacked(
                    "data:application/json;base64,",
                    base64(bytes(metadata))
                )
            );
    }

    string internal constant TABLE =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    function base64(bytes memory data) internal pure returns (string memory) {
        if (data.length == 0) return "";
        
        string memory table = TABLE;        
        uint256 encodedLen = 4 * ((data.length + 2) / 3);        
        string memory result = new string(encodedLen + 32);

        assembly {
            
            mstore(result, encodedLen)            
            let tablePtr := add(table, 1)           
            let dataPtr := data
            let endPtr := add(dataPtr, mload(data))            
            let resultPtr := add(result, 32)
            
            for {

            } lt(dataPtr, endPtr) {

            } {
                dataPtr := add(dataPtr, 3)
                
                let input := mload(dataPtr)                
                mstore(
                    resultPtr,
                    shl(248, mload(add(tablePtr, and(shr(18, input), 0x3F))))
                )
                resultPtr := add(resultPtr, 1)
                mstore(
                    resultPtr,
                    shl(248, mload(add(tablePtr, and(shr(12, input), 0x3F))))
                )
                resultPtr := add(resultPtr, 1)
                mstore(
                    resultPtr,
                    shl(248, mload(add(tablePtr, and(shr(6, input), 0x3F))))
                )
                resultPtr := add(resultPtr, 1)
                mstore(
                    resultPtr,
                    shl(248, mload(add(tablePtr, and(input, 0x3F))))
                )
                resultPtr := add(resultPtr, 1)
            }            
            switch mod(mload(data), 3)
            case 1 {
                mstore(sub(resultPtr, 2), shl(240, 0x3d3d))
            }
            case 2 {
                mstore(sub(resultPtr, 1), shl(248, 0x3d))
            }
        }

        return result;
    }
}