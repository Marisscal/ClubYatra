// SPDX-License-Identifier: MIT LICENSE

pragma solidity ^0.8.0;

interface IYATRA {
    // struct to store each token's traits
    struct TraitStruct {
        bool isPP;       
        
    }

    function getPaidTokens() external view returns (uint256);

    function getTokenTraits(uint256 tokenId)
        external
        view
        returns (TraitStruct memory);  

    
    function balanceHolder(address user)
        external
        view        
        returns (uint256);
}